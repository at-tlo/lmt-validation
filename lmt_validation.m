function lmt_validation()

disp('INFO: Start LMT validation')

%Directory where the raw measurement files are located.
%Fileformat should be ECC 05/01
input_data_dir = '/data/ssd/ecc-05-01-not-validated/';

%Directory where the matlab files are stored.
output_data_dir = '/data/ssd/matlab/';

%Directory where the invalid files are stored
output_invalid_dir = '/data/ssd/ecc-05-01-invalid/';

%Validion results directory
validation_results_dir = '/data/ssd/validation_results/';

%Validation results filename, create new file and write the headers to it. 
validation_results_filename = [validation_results_dir, 'validation_results-',char(datetime("today"), "yyyy-MM-dd"),'.csv'];
table_header = cell2table(cell(0,10), 'VariableNames', {'date','location','ref_freq','ref_value','ref_variation','valid','measured_min','measured_max','measured_median','measured_std'});
writetable(table_header, validation_results_filename,'WriteVariableNames',true,'WriteRowNames',false)

%Read the ref values as matlab table
ref_value_table = readtable('ref_values.csv');

%Get all the location that should be validated
%e.g. They have a ref value in the ref table.
locations_have_ref_value = unique(ref_value_table.Location);

%Get all the filenames in the input dir.
%All filenames should have the following structure.
%YYYY-MM-DD-LocationName-StartFreqInKhz-StopFreqInKhz.txt
all_filenames = dir([input_data_dir, '*txt']);

%Preallocate the measurement struct
measurement = struct( ...
    'date',cell(1,length(all_filenames)),...
    'location',cell(1,length(all_filenames)),...
    'startFreq',cell(1,length(all_filenames)),...
    'stopFreq',cell(1,length(all_filenames)),...
    'valid',cell(1,length(all_filenames))...
    );

for i = 1:length(all_filenames)
    disp(['INFO: Processing ', all_filenames(i).name])
    %Split the filename to table.
    filename_without_extention = strrep(all_filenames(i).name,'.txt','');
    filename_split = split(filename_without_extention,'-');

    if length(filename_split) ~= 6
        %file has wrong format continue
        continue
    end
    %Convert the measurement date in the filename to a MATLAB datetime
    %format.
    measurement(i).date = datetime( ...
        str2double(filename_split(1)), ...
        str2double(filename_split(2)), ...
        str2double(filename_split(3)));

    %Get the Measurement location form the filename.
    measurement(i).location = filename_split(4);

    %check if location have a ref value
    %We assume that a location that have not a ref value, is not valid.
    if ~any(strcmp(measurement(i).location,locations_have_ref_value))
        %location have no ref value. Set the valid flag.
        measurement(i).valid = 0; %no ref found!
    end

    %Get the measurement start and stop Frequency and covert it to Hz.
    measurement(i).startFreq = str2double(filename_split(5)) * 1000;
    measurement(i).stopFreq = str2double(filename_split(6)) * 1000;
end

%Get all the measurement locations present in the measurement files.
all_measurement_locations = unique([measurement.location]);


%% Do the validation for every measurement location
for i = 1:length(all_measurement_locations)
    %check if location have a ref value
    %We assume that a location that have not a ref value, is not valid.
    %We can continue as the measurent invalidated in the previous step.

    if ~any(strcmp(all_measurement_locations(i),locations_have_ref_value))
        %NO ref value found!
        disp(['WARNING: No ref value found for location: ', ...
            char(all_measurement_locations(i)),...
            '. This location will be skiped!'...
            ])
        continue
    end
    %get the location index in the measurement struct.
    logical_array_location = strcmp( ...
        all_measurement_locations(i),[measurement.location]);
    index_location = find(logical_array_location);

    %find the unique measurement dates for the current location.
    unique_dates = unique([measurement(index_location).date]);

    for j = 1:length(unique_dates)

        disp(['INFO: Start validation for location ' ...
            char(all_measurement_locations(i)),...
            ', measurement date ',...
            char(unique_dates(j))...
            ])

        %get the measurement indexes
        index_measurements = find(...
            unique_dates(j) ==[measurement.date] & ...
            logical_array_location);
        %loop over all measurement and check if the measurement contains a
        %ref frequency.
        %Count the number of valid measurements.
        valid_measurement_count = 0;
        for k = 1:length(index_measurements)
            %find the ref frequency index
            logical_array_ref = strcmp( ...
                all_measurement_locations(i),[ref_value_table.Location]);

            %get the ref frequencies in Hz, output in row vector
            ref_freqs = ref_value_table.Freq(logical_array_ref)';
            ref_values = ref_value_table.Value(logical_array_ref)';
            ref_variation = ref_value_table.Variation(logical_array_ref)';

            %loop over all ref frequencies
            for l = 1:length(ref_freqs) 
                full_filename = [input_data_dir, all_filenames(index_measurements(k)).name];
                if (ref_freqs(l) > measurement(index_measurements(k)).startFreq ...
                        && ref_freqs(l) < measurement(index_measurements(k)).stopFreq)
                    %ref freq is in measurement, do validation
                    disp(['INFO: Start validation on ref freq ', num2str(ref_freqs(l)/10^6),'MHz'])

                    [valid,min_ref_freq,max_ref_freq,median_ref_freq,std_ref_freq] = ...
                        validate_file(full_filename,ref_freqs(l),ref_values(l),ref_variation(l));
                    if valid ==1
                        %file is valid
                        measurement(i).valid = 1;
                        valid_measurement_count = valid_measurement_count + 1;
                        disp(['INFO: Valid at ref freq ', num2str(ref_freqs(l)/10^6),'MHz'])
                    else
                        %file is not valid
                        disp(['WARNING: NOT valid at ref freq ', num2str(ref_freqs(l)/10^6),'MHz'])
                        measurement(i).valid = 0;
                    end
                    %display and save valiation results.
                    valdation_results_table = table( ...
                        measurement(index_measurements(k)).date, ...
                        measurement(index_measurements(k)).location, ...
                        ref_freqs(l), ...
                        ref_values(l), ...
                        ref_variation(l), ...
                        valid, ...
                        min_ref_freq, ...
                        max_ref_freq, ...
                        median_ref_freq, ...
                        std_ref_freq);
                    %save to csv
                    writetable(valdation_results_table, ...
                        validation_results_filename, ...
                        'WriteMode','Append',...
                        'WriteVariableNames',false,...
                        'WriteRowNames',false)
                    %save to influx
                    dateEpoch =  convertTo(measurement(index_measurements(k)).date,'epochtime','Epoch','1970-01-01','TicksPerSecond',1);
                    write_to_influx( ...
                        dateEpoch, ...
                        char(measurement(index_measurements(k)).location), ...
                        ref_freqs(l), ...
                        ref_values(l), ...
                        ref_variation(l), ...
                        valid, ...
                        min_ref_freq, ...
                        max_ref_freq, ...
                        median_ref_freq, ...
                        std_ref_freq);
                    disp(['INFO: Expected value: ', num2str(ref_values(l)), 'dBuV/m'])
                    disp(['INFO: Measured min: ', num2str(min_ref_freq), 'dBuV/m'])
                    disp(['INFO: Measured max: ', num2str(max_ref_freq), 'dBuV/m'])
                    disp(['INFO: Measured median: ', num2str(median_ref_freq), 'dBuV/m'])
                    disp(['INFO: Measured standard diviation: ', num2str(std_ref_freq), 'dB'])
                end
            end
        end

        disp(['INFO: Validation done for location ' ...
            char(all_measurement_locations(i)),... 
            ', measurement date ',...
            char(unique_dates(j))...
            ])
        %% Validation for done for 1 date for 1 measurement location.
        % Files can be copied.
        % Check if the number valid files is equal to the number of ref
        % frequencies. We excect 1 measurement per ref frequecy. Multiple
        % measurements per ref frequency is currently not supported.
        if valid_measurement_count == length(ref_freqs)
            %All measurements assumed valid
            %Loop over all measurements for the current date and location.
            %Save the output as matlab file.
            disp('INFO: Start matlab file creation in parralel.')
            parfor k = 1:length(index_measurements)
                input_filename = [input_data_dir, all_filenames(index_measurements(k)).name];
                disp(['INFO: Create matlab file for ', input_filename])
                matlab_data_struct = convert_ecc_0501_to_mat(input_filename);
                yearMeasurement = num2str(year(matlab_data_struct.Date));
                output_filename = [output_data_dir, yearMeasurement , '/' , matlab_data_struct.LocationName, '/' , strrep(all_filenames(index_measurements(k)).name,'.txt','.mat')];
                write_measurement_info_to_lmt_api(matlab_data_struct,output_filename);
                par_save(output_filename,matlab_data_struct)
                delete(input_filename)
            end
        else
            %Move all the file to the not valid dir.
            for k = 1:length(index_measurements)
                input_filename = [input_data_dir, all_filenames(index_measurements(k)).name];
                disp(['INFO: Move file to not valid directory: ', input_filename])
                movefile(input_filename,output_invalid_dir)
            end
        end
    end
end
end
