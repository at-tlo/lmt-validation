% Save function for file within a par loop
% Transparency is violated by the normal SAVE command because in general MATLAB 
% cannot determine which variables from the workspace will be saved to a file.
% See https://nl.mathworks.com/matlabcentral/answers/135285-how-do-i-use-save-with-a-parfor-loop-using-parallel-computing-toolbox
function par_save(filename, data)
directory = fileparts(filename);
    if ~exist(directory, 'dir')
       mkdir(directory)
    end
  save(filename, 'data','-mat','-v7.3')
end