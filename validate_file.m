function [valid, min_value, max_value, median_value, std_value] = validate_file(filename, ref_freq, ref_value, max_variation)
%VALIDATE_FILE Validates a measurement file

%Convert the data to matlab datafile.
data = convert_ecc_0501_to_mat(filename);

%Get the correct collum
collum_ref_freq = (ref_freq - data.FreqStart)/data.FreqStep + 1;

%Get the min,max,median and standard divation.
min_value = min(data.Measurements(:,collum_ref_freq),[],'omitnan');
max_value = max(data.Measurements(:,collum_ref_freq),[],'omitnan');
median_value = median(data.Measurements(:,collum_ref_freq),'omitnan');
std_value = std(data.Measurements(:,collum_ref_freq),'omitnan');

%valid if
% - Measured median have a maximum value of ref_value + max_variation.
% If the measured value exceeds this maximum, the measured S11 is higher
% then excepted. A lower measured S11 is not a problem for now.
%
% - Minimum value is larger then zero.
% To verify that the test source was on during the measurement.

if (median_value - ref_value) < max_variation && min_value > 0
    %data is valid
    valid = 1;
else
    %data is not valid
    valid = 0;
end
end

