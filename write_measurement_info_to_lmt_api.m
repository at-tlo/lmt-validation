%% Write data to influx
function response = write_measurement_info_to_lmt_api(data,filename)
load ('lmt_api_config.mat','config_lmt_api')
uri = config_lmt_api.uri;
measurement_info.Date = strcat(string(data.Date,'yyyy-MM-dd') + 'T00:00:00Z');
measurement_info.FileType = data.FileType;
measurement_info.Location = data.LocationName;
measurement_info.Latitude = data.Latitude;
measurement_info.Longitude = data.Longitude;
measurement_info.FreqStart = data.FreqStart;
measurement_info.FreqStop = data.FreqStop;
measurement_info.FreqStep = data.FreqStep;
measurement_info.LevelUnits = data.LevelUnits;
measurement_info.DataPoints = data.DataPoints;
measurement_info.FileName = filename;
measurement_info.AntennaType = data.AntennaType;
measurement_info.FilterBandwidth = data.FilterBandwidth;
measurement_info.Detector = data.Detector;
measurement_info.TotalMeasurementTime = data.TotalMeasurementTime; 
measurement_info.MeasurementsPerHour = data.MeasurementsPerHour;
measurement_info.HoursWithMeasurement = data.HoursWithMeasurement;
measurement_info.MinutesWithMeasurement = data.MinutesWithMeasurement;
measurement_info.SecondsWithMeasurement = data.SecondsWithMeasurement;

body = jsonencode(measurement_info);
options = weboptions(...
    'MediaType', 'text/plain; charset=utf-8',...
    'ContentType', 'json',...
    'Timeout', 30, ...
    'HeaderFields', {'Authorization' ['Bearer ' config_lmt_api.token]}...
    );
response = webwrite(uri, body, options);
% while 1
%     try
%         response = webwrite(uri, body, options);
%         disp("INFO: Write to API falied done.")
%         break
%     catch
%         disp("WARNING: Write to inluxDB failed try again in 10 seconds.")
%         pause(10)
%     end
% end
end